package parse

import (
	"encoding/json"
	"lesson1/blockchain"
	"log"
	"reflect"
)

type respOk struct {
	Code int `json:"code"`
	Data interface{} `json:"data"`
	Message string `json:"message"`
}

type RNodesParams struct {
	Url string
}

func VerifyTransaction(t blockchain.Transaction) bool  {
	tValue := reflect.ValueOf(t)
	tType := reflect.TypeOf(t)
	for i := 0; i < tValue.NumField(); i++ {
		fType := tType.Field(i).Type.String()

		switch fType {
		case "string":
			fValue := tValue.Field(i).String()
			if fValue == "" {
				return false
			}
		case "float64":
			fValue := tValue.Field(i).Float()
			if fValue == 0 {
				return false
			}
		}
	}

	return true
}
func RespOk(code int, data interface{}, message string)[]byte  {
	bytes, err := json.Marshal(respOk{
		Code:    code,
		Data:    data,
		Message: message,
	})
	if err != nil {
		log.Println(err.Error())
		return nil
	}
	return bytes
}